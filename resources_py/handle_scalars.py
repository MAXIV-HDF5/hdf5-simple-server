#!/usr/bin/python

'''
Decimate large images so that they can be sent
'''

#####################
# IMPORT LIBRARIES ##
#####################

# HDF5
import hdf5plugin
import h5py

assert hdf5plugin  # silence pyflakes


###########
# HELPERS #
###########

def get_number(fileName, dataset_name, slices, debug):
    '''
    '''

    if debug:
        print('')
        print('*** START resources/handle_scalars.py get_number ***')
        print('')

    # Get a line from a file
    f1 = h5py.File(fileName, 'r')

    image = f1[dataset_name][()]

    f1.close()

    if debug:
        print('')
        print('*** END resources/handle_scalars.py get_number ***')
        print('')

    return image
