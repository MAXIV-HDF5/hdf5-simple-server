#!/usr/bin/env python

'''
Handle all requests for the fetching of file contents & datasets.
'''

#####################
# IMPORT LIBRARIES ##
#####################

# HDF5
import hdf5plugin
import h5py

# General systems
import argparse
import sys
import ast
import os.path
import os
import subprocess

# json
import json

# Profiling
import cProfile

assert hdf5plugin  # silence pyflakes


###########
# HELPERS #
###########

CONFIG_PATH = ""


def ignore_file(full_path, debug):

    if debug:
        print('')
        print('*** START resources_py/handle_object.py ignore_file ***')
        print('')
        print('full_path : ' + str(full_path))

    ignore_file = False
    hdf5_file_extensions = ['.h5', '.hdf5', '.hdf', '.nxs', '.nx5']

    # Check if this is a file (maybe with no extension?) or folder
    item_type = False
    if os.path.isfile(full_path):
        item_type = 'file'
    if os.path.isdir(full_path):
        item_type = 'folder'

    if debug:
        print('item_type:    ', item_type)

    # Get the last bit of the path name
    base_name = os.path.basename(full_path)
    if debug:
        print('base_name:     [' + str(base_name) + ']')

    # See what kind of extension may or may not be present
    fileName, fileExtension = os.path.splitext(base_name)
    if debug:
        print('fileName:      [' + str(fileName) + ']')
        print('fileExtension: [' + str(fileExtension) + ']')

    # If this is a file (rough guess), see if it is an hdf5 file (rough guess)
    # if fileExtension:
    if item_type == 'file':
        if fileExtension not in hdf5_file_extensions:
            ignore_file = True

    # Check if this is a hidden file or folder, which will be ignored
    if fileName.startswith('.'):
        ignore_file = True

    if debug:
        print('')
        print('ignore_file: ', ignore_file)
        print('')
        print('*** END resources_py/handle_object.py ignore_file ***')
        print('')

    return ignore_file


def parse_object_path(input_string, debug):

    """
    This function figures out what is in the given input string:
        - A folder
        - An hdf5 file
        - A dataset within an hdf5 file
        - A folder within an hdf5 file
    """

    if debug:
        print('')
        print('*** START resources_py/handle_object.py parse_object_path ***')
        print('')
        print('input_string : ' + str(input_string))

    object_type = file_path = h5_path = slice_par = False

    # Strip possible trailing characters
    input_string = input_string.rstrip('/ \\')

    # Possible hdf5 file name extensions
    hdf5_file_extensions = ['.h5', '.hdf5', '.hdf', '.nxs', '.nx5']

    # Check if this is a file or folder that should be ignored
    if ignore_file(input_string, debug):

        object_type = 'ignore_file'

    else:

        # Check if this string ends with an hdf5 file name
        if input_string.endswith(tuple(hdf5_file_extensions)):

            object_type = 'h5_file'
            file_path = input_string

        else:

            # Now see if it contains a file name plus some object path
            for ext in hdf5_file_extensions:

                # Split the string on the extension plus trailing slash
                delim = ext + '/'
                if delim in input_string:

                    split_string = input_string.split(delim, 1)

                    # The file path is the first piece
                    file_path = split_string[0] + ext

                    # The path within the file is the rest
                    h5_path = '/' + split_string[1]

                    object_type = 'h5_object'

            # If there was no file extension in the string, then this is
            # probably a simple folder path
            if not object_type:

                object_type = 'folder'
                file_path = input_string

            # Check if there is image slice notation in the h5_path, if so,
            # save to a list - there must be a smarter way to do this :)
            if h5_path:

                # The slice notation should be enclosed by brackets
                if '[' and ']' in h5_path:

                    if debug:
                        print('image slice notation?')

                    # Look for the slice notation
                    slice_string = \
                        h5_path[h5_path.find("[")+1:h5_path.find("]")]

                    # Split at commas
                    split_string = slice_string.split(',')

                    if debug:
                        print('  len(split_string):', len(split_string))
                        print('  split_string:', split_string)

                    # Create a list into which the split parameter will be
                    # saved
                    slice_par = [None]*(len(split_string)*2)

                    for i, elem in enumerate(split_string):
                        if debug:
                            print('  elem:', elem)

                        # Look for colons separating slice parameters
                        split_par = elem.split(':')

                        if debug:
                            print('  split_par:', split_par)

                        # Save each slice parameter
                        slice_par[i*2] = int(split_par[0])
                        slice_par[i*2 + 1] = int(split_par[1])

                    # Remove the slice notation from h5_path
                    split_string = h5_path.split('[', 1)
                    h5_path = split_string[0]

                    if debug:
                        print('  slice_par:', slice_par)

    if debug:
        print('')
        print('  object_type  : ' + str(object_type))
        print('  file_path    : ' + str(file_path))
        print('  h5_path      : ' + str(h5_path))
        print('  slice_par    : ' + str(slice_par))
        print('')
        print('*** END resources_py/handle_object.py parse_object_path ***')
        print('')

    return object_type, file_path, h5_path, slice_par


def get_folder_contents(file_path, debug):

    if debug:
        print('')
        print('*** START resources_py/handle_object.py get_folder_contents **')
        print('')

    # Create output dictionary
    contents = {}
    contents['folder_contents'] = {}

    # Get the folder contents
    item_list = []
    try:
        item_list = os.listdir(file_path)
    except OSError:
        print('shit - no access to file?')

    # Read the config file in order to make urls
    file_url = create_file_url(file_path, debug)

    # Loop over items, add to output dictionary
    i = 0
    for item in item_list:

        if debug:
            print('**** Start loop over item in folder', i, '***')
            print('  item:     ', item)

        short_name = False
        full_path = False
        item_url = False
        item_type = False
        ignore_item = False
        can_user_read_file = False

        # Assemble the complete file path, note type of file. There should only
        # be two possibilities: folder or hdf5 file. Other file types need to
        # be ignored with ignore_file()
        full_path = file_path + '/' + item
        if os.path.isfile(full_path):
            item_type = 'h5_file'
        if os.path.isdir(full_path):
            item_type = 'folder'

        if debug:
            print('  item_type:', item_type)

        # Check if this is a file or folder that should be ignored.
        ignore_item = ignore_file(full_path, debug)

        if item_type and not ignore_item:

            # Check if the user executing this process  has read access for
            # this file
            can_user_read_file = os.access(full_path, os.R_OK)

            if debug:
                print('')
                print('full_path:         ', full_path)
                print('can_user_read_file:', can_user_read_file)
                print('')

        # Not sure if I should return filenames not readable by user, or just
        # leave them out... - leave them out for now
        if can_user_read_file:

            # Create the url for this item
            item_url = file_url + '/' + item

            # Create a shortened version of the name - just the file name,
            # folder, or item within an hdf5 file
            short_name = os.path.basename(item)
            if debug:
                print('  short_name:     [' + str(short_name) + ']')

            contents['folder_contents'][i] = {}
            contents['folder_contents'][i]['short_name'] = short_name
            contents['folder_contents'][i]['full_name'] = full_path
            contents['folder_contents'][i]['url'] = item_url
            contents['folder_contents'][i]['item_type'] = item_type
            contents['folder_contents'][i]['user_can_read'] = \
                can_user_read_file
            i += 1

        if debug:
            print('**** End loop over item ***')
            print('')

    if debug:
        print('')
        print(json.dumps(contents, indent=2, sort_keys=True))
        print('')
        print('*** END resources_py/handle_object.py get_folder_contents ***')
        print('')

    return contents


def print_attrs(name, obj):
    print(name)
    for key, val in obj.attrs.items():
        print("    %s: %s" % (key, val))


def get_h5_file_folder_contents(file_path, h5_path, data_type, debug):

    """
    Check this shit out:
    http://docs.h5py.org/en/latest/high/group.html#reference
    """

    if debug:
        print('')
        print('*** START resources_py/handle_object.py '
              'get_h5_file_folder_contents ***')
        print('')

    h5_file = h5_object = False

    # Create output dictionary
    contents = {}
    contents['folder_contents'] = {}

    # Open the file
    try:
        h5_file = h5py.File(file_path, 'r')
    except IOError:
        h5_file = False
        print('not an hdf5 file? empty file?')
    except Exception:
        h5_file = False
        print('Unexpected error:')

    # Get the folder in the file if requested, otherwise just open the file
    if h5_file and h5_path:
        h5_object = h5_file[h5_path]
    else:
        h5_object = h5_file

    if debug:
        print('')
        if h5_object:
            print('items:')
            h5_file = h5py.File(file_path, 'r')
            print('')
            print('values:')
            print(list(h5_object.values()))
            print('')
            print('keys:')
            print(list(h5_object.keys()))
            # h5_object.visititems(print_attrs)
            # print ''
        else:
            print('h5_file:  ', h5_file)
            print('h5_object:', h5_object)
        print('')

    # Read the config file in order to make urls
    file_url = create_file_url(file_path, debug)

    # Loop over items, add to output dictionary
    i = 0

    if h5_object:
        for key in list(h5_object.values()):

            item_type, shape = is_h5_group_or_dataset(key, debug)

            if debug:
                print('***')
                print('key: ', key)

            if key is None:
                if debug:
                    print('*** BAD KEY - SKIPPING ***')
                continue

            if item_type == 'text-array':
                item_type = 'text'

            if debug:
                print('key.name:  [', key.name, ']')
                print('item_type: [', item_type, ']')

            # The full path name on the host system (key.name contains a slash
            # at the beginning)
            full_path = file_path + key.name

            # Create the url for this item
            item_url = file_url + key.name

            # Create a shortened name from the full path name
            split_string = key.name.lstrip('/ \\')
            split_string = split_string.rstrip('/ \\')
            split_string = split_string.split('/')
            short_name = split_string[-1]

            # The output dictionary which will eventually be sent as json
            contents['folder_contents'][i] = {}
            contents['folder_contents'][i]['short_name'] = short_name
            contents['folder_contents'][i]['full_name'] = full_path
            contents['folder_contents'][i]['url'] = item_url
            contents['folder_contents'][i]['item_type'] = item_type
            contents['folder_contents'][i]['shape'] = shape
            contents['folder_contents'][i]['user_can_read'] = True

            i += 1

    if h5_file:
        h5_file.close()

    if debug:
        print('')
        print(json.dumps(contents, indent=2, sort_keys=True))
        print('')
        print('*** END resources_py/handle_object.py '
              'get_h5_file_folder_contents ***')
        print('')

    return contents


def does_object_exist(object_type, file_path, h5_path, debug):

    """
    This function determines whether or not a given item exists, which could
    be one of the following
        - A folder on disk
        - An hdf5 file
        - An dataset within an hdf5 file
        - A folder within an hdf5 file

    If the object is within an hdf5 file, the type of item is determined:
        - folder within the hdf5 file
        - number
        - string
        - image
        - image series
        - line (array)
    """

    if debug:
        print('')
        print('*** START resources_py/handle_object.py does_object_exist ***')
        print('')
        print('file_path:    ', file_path)

    object_exists = False
    path_exists = False
    data_type = False
    shape = False
    f1 = False

    # Check if the file or folder path exists
    if file_path:
        path_exists = os.path.exists(file_path)

    if object_type == 'h5_object' and path_exists:

        if debug:
            print('checking for h5 object')

        # Open the file
        try:
            f1 = h5py.File(file_path, 'r')

            # See if the given h5 object exists
            object_exists = h5_path in f1
        except IOError:
            print('not so fast, hotshot')

        if object_exists:

            if debug:
                print('the object ' + str(h5_path) + ' exists')

            # If this is an HDF5 data object, check if it's a group or dataset
            # - are there other possibilities as well?
            try:
                h5_object = f1[h5_path]
                if debug:
                    print('h5_object.name: ' + str(h5_object.name))
                data_type, shape = is_h5_group_or_dataset(h5_object, debug)
            except KeyError:
                h5_object = False

        else:
            if debug:
                print('the object ' + str(h5_path) + ' does not exist')

        if f1:
            f1.close()

    else:
        object_exists = path_exists

    if debug:
        print('object_exists: ' + str(object_exists))
        print('data_type:     ' + str(data_type))
        print('shape:        ', shape)

        print('')
        print('*** END resources_py/handle_object.py does_object_exist ***')
        print('')

    return object_exists, data_type, shape


def is_h5_group_or_dataset(h5_object, debug):

    """
    If the object is within an hdf5 file, the type of item is determined:
        - folder within the hdf5 file
        - number
        - string
        - image
        - image series
        - line (array)
    """

    if debug:
        print('')
        print('*** START resources_py/handle_object.py is_h5_group_or_dataset')
        print('')

    data_type = shape = False

    # Need to see if this is a folder (group) or data (dataset)
    # Are there other possibilities?
    try:
        h5_object_data_type = h5_object.dtype

        if debug:
            print('')
            print('h5_object.dtype: ' + str(h5_object_data_type))
            print('h5_object.shape: ' + str(h5_object.shape))
            print('h5_object.dims: ' + str(h5_object.dims))
            for dim in h5_object.dims:
                print('dim.label: ' + str(dim.label))
            print('h5_object.file: ' + str(h5_object.file))
            print('h5_object.parent: ' + str(h5_object.parent))
            print('h5_object.attrs: ' + str(h5_object.attrs))

        shape = h5_object.shape
        shape_length = len(shape)

        # See what the hell we've got here - this part could be done better...
        if shape_length == 0:
            if 'float' in str(h5_object.dtype) or \
                    'int' in str(h5_object.dtype):
                data_type = 'number'
            else:
                data_type = 'text'

        if shape_length == 1:
            data_type = 'line'

            # Sometimes strings are stored in an array, try to look for
            # such cases
            if debug:
                print('shape[0]: ', shape[0])
            if shape[0] == 1:
                array = h5_object[:]
                if debug:
                    print('array:    ', array)
                    print('dtype:    ', array.dtype)
                string_value = array[0]
                is_string = isinstance(string_value, str)
                if debug:
                    print('array[0]: ', string_value)
                    print('is_string:', is_string)
                if is_string:
                    data_type = 'text-array'

                if debug:
                    print('Attributes for h5_object?')
                    for key, val in h5_object.attrs.items():
                        print("    %s: %s" % (key, val))
                    print('')

        if shape_length == 2:
            data_type = 'image'
        if shape_length == 3:
            data_type = 'image-series'

    except AttributeError:
        data_type = 'h5_folder'
        shape = 0

    if debug:
        print('')
        print('data_type: ' + str(data_type))
        print('')
        print('*** END resources_py/handle_object.py is_h5_group_or_dataset')
        print('')

    return data_type, shape


def convert_data_to_dict_list(path, dataset, object_type, data_type, shape,
                              section, bpr_limits, bad_pixels_exist,
                              is_downsampled, debug):

    dataset_dict = {}
    dataset_dict['path'] = path

    if debug:
        print('')
        print('*** START resources_py/handle_object.py '
              'convert_data_to_dict_list ***')
        print('')

    if dataset is False:
        dataset_dict['user_can_read'] = False
        return dataset_dict

    if object_type == 'h5_object':

        dataset_dict['object_type'] = object_type
        dataset_dict['item_type'] = data_type
        dataset_dict['user_can_read'] = True

        if data_type == 'image-series' or data_type == 'image' or \
                data_type == 'line':
            dataset_dict['shape'] = shape
            dataset_dict['values'] = dataset.tolist()
            dataset_dict['bpr_limits'] = bpr_limits
            dataset_dict['section'] = section
            dataset_dict['bad_pixels_exist'] = bad_pixels_exist
            dataset_dict['is_downsampled'] = is_downsampled

        if data_type == 'number':
            dataset_dict['values'] = dataset.tolist()

        if data_type == 'text' or data_type == 'text-array':
            if debug:
                print(' is this text a string?:', isinstance(dataset, str))
            if isinstance(dataset, str):
                dataset_dict['values'] = dataset
            else:
                dataset_dict['values'] = str(dataset.decode())

            # quick fix - need to fill these in properly!
            dataset_dict['error'] = 'False'
            dataset_dict['item_type'] = 'text'
            dataset_dict['does_exist'] = 'True'
            dataset_dict['short_name'] = os.path.basename(dataset_dict['path'])

        if data_type == 'h5_folder':
            dataset_dict.update(dataset)

    if object_type == 'h5_file' or object_type == 'folder':
        dataset_dict['object_type'] = object_type
        dataset_dict['user_can_read'] = True
        dataset_dict.update(dataset)

    if debug:
        print('')
        print('*** END resources_py/handle_object.py '
              'convert_data_to_dict_list ***')
        print('')

    return dataset_dict


def read_config_file(debug):

    if debug:
        print('')
        print('*** START resources_py/handle_object.py read_config_file ***')
        print('')

    # Dumb way to get the config options?
    config_par = {}
    with open(CONFIG_PATH) as myfile:
        for line in myfile:
            name, var = line.partition("=")[::2]
            name = name.strip()
            var = var.strip()
            var = str(var.replace("'", ""))
            var = var.rstrip('/ \\')
            config_par[name] = var

    data_dir = protocol = host_name = port = False

    if 'DATA_DIR' in config_par:
        data_dir = config_par['DATA_DIR']
    if 'PROTOCOL' in config_par:
        protocol = config_par['PROTOCOL']
    if 'HOST_NAME' in config_par:
        host_name = config_par['HOST_NAME']
    if 'PORT' in config_par:
        port = config_par['PORT']

    if debug:
        print('  data_dir:  ', data_dir)
        print('  protocol:  ', protocol)
        print('  host_name: ', host_name)
        print('  port:      ', port)
        print('')
        print('*** END resources_py/handle_object.py read_config_file ***')
        print('')

    # return data_dir, protocol, host_name, port
    return config_par


def create_file_url(file_path, debug):

    config_par = read_config_file(debug)
    data_dir = protocol = host_name = port = False

    if 'DATA_DIR' in config_par:
        data_dir = config_par['DATA_DIR']
    if 'PROTOCOL' in config_par:
        protocol = config_par['PROTOCOL']
    if 'HOST_NAME' in config_par:
        host_name = config_par['HOST_NAME']
    if 'PORT' in config_par:
        port = config_par['PORT']

    # I'm making the urls without the data directory name, so remove it.
    # The resulting url will not be sensible if this script is run from the
    # command line and the given path does not match that in the config file
    file_path_in_data_dir = ''
    if data_dir in file_path:
        file_path_in_data_dir = file_path.split(data_dir, 1)[1]
    else:
        file_path_in_data_dir = file_path

    # Construct file url
    file_url = protocol + '://' + host_name + ':' + port + \
        file_path_in_data_dir

    return file_url


def execute_subprocess_as_user(args, userid, usergid, usergids, debug):

    """
    Execute a python script as the specified user
    """

    if debug:
        print('*** START resources_py/handle_object.py '
              'execute_subprocess_as_user ***')
        print('')
        print('  ============================================================')
        print('  args:     ', args)
        print('  userid:   ', userid)
        print('  usergid:  ', usergid)
        print('  usergids: ', usergids)
        print('  ============================================================')
        print('')

    output = rc = error = result = False

    # Execute the subprocess
    process = subprocess.Popen(args,
                               preexec_fn=demote(userid, usergid, usergids),
                               shell=False, bufsize=-1,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    # Get the output
    output, error = process.communicate()
    process.wait()

    # 0 indicates that it ran successfully.
    rc = process.returncode

    if not isinstance(output, str):
        output = str(output.decode())

    if debug:
        print('  ============================================================')
        print('  output: ', output)
        print('  rc:     ', rc)
        print('  error:  ', error)
        print('  result: ', result)
        print('  ============================================================')
        print('')
        print('*** END resources_py/handle_object.py '
              'execute_subprocess_as_user ***')

    return output


def demote(user_uid, user_gid, usergids):

    """
    Pass the function 'set_ids' to preexec_fn, rather than just calling
    setuid and setgid. This will change the ids for that subprocess only
    """

    def set_ids():

        # Need to set groups ids first, then uid for some reason
        # os.setgroups(usergids)
        os.setgid(int(user_gid))
        os.setuid(int(user_uid))

    return set_ids


def start_profiling(run_profile):

    pr = False

    # Start profiling
    if run_profile:
        pr = cProfile.Profile()
        pr.enable()

    return pr


def stop_profiling(pr, output_title):

    # Stop profiling, print results
    if pr:
        print('')
        print('==============================================================')
        print(output_title)
        print('')
        pr.disable()
        pr.print_stats(sort='time')
        print('==============================================================')
        print('')


def get_object_as_user(input_string, username, get_tree_path, force_no_cpp,
                       debug):

    if debug:
        print('')
        print('*** START resources_py/handle_object.py get_object_as_user ***')
        print('')
        print('Check who is running this script (should be root):')
        print('  os.getuid():  ', os.getuid())
        print('  os.getgid():  ', os.getgid())
        print('')

    # Get the user uid and gid numbers from the system
    userid, usergid, usergids = get_user_info.get_user_info_sys(username,
                                                                debug)

    if debug:
        print('Subprocess will be executed as the following user:')
        print('  username:     ', username)
        print('  userid:       ', userid)
        print('  usergid:      ', usergid)
        print('  usergids:     ', usergids)
        print('')

        # Execute a couple test processes as the logged in user
        args = ['id']
        execute_subprocess_as_user(args, userid, usergid, usergids, debug)
        if debug:
            print('')

        args = [sys.executable, '--version']
        execute_subprocess_as_user(args, userid, usergid, usergids, debug)
        if debug:
            print('')

    path = input_string

    # Get a couple configuration options
    config_par = read_config_file(debug)
    data_dir = use_cpp_subprocess = False
    if 'DATA_DIR' in config_par:
        data_dir = config_par['DATA_DIR']
    if 'USE_CPP_SUBPROCESS' in config_par:
        use_cpp_subprocess = config_par['USE_CPP_SUBPROCESS']

    # In case we want to force the use of the python sub process
    if force_no_cpp:
        use_cpp_subprocess = False

    # The base data directory name is set in the config file, it's not used
    # in the urls, but it is needed for data retrieval
    if (path.find(data_dir[1:]) == -1):
        # Prepend the base data directory path
        path = os.path.join(data_dir, path)
    else:
        # Prepend a slash
        path = os.path.join('/', path)

    if debug:
        print('full path: [' + str(path) + ']')

    # Assemble the command and arguments, the ordering is important
    if use_cpp_subprocess:
        if debug:
            print('Running c++ subprocess')
            print('')
        app_dir = os.path.dirname(os.path.abspath(__file__))
        exe_path = os.path.join(app_dir, '../resources_cpp/bin/handle_object')
        args = [exe_path, path, '-j', '-m', '1.6e5', '-c', CONFIG_PATH]
    else:
        if debug:
            print('Running python subprocess')
            print('')
            print('sys.executable: ', sys.executable)
            print('')
        args = [sys.executable, 'resources_py/handle_object.py', path, '-s']

    if get_tree_path:
        args.append('-t')

    if debug:
        args.append('-d')

    pr = start_profiling(False)

    # Execute the subprocess as the logged in user
    output = execute_subprocess_as_user(args, userid, usergid, usergids, debug)

    stop_profiling(pr, 'profile of execute_subprocess_as_user')

    # Look for a substring between two keywords in the output
    keyword = "OUTPUT_DICTIONARY"
    output = output[output.find(keyword)+len(keyword):output.rfind(keyword)]

    # Convert the output string to a dictionary
    # Running ast.literal_eval seems to take about 45 ms with 10k pixel images
    output_dictionary = ast.literal_eval(output)

    #############################
    # TEMPORARY FIX - hopefully #
    #############################
    # There are sometimes strange arrays which contain string values, as well
    # as simple string values which for some reason I cannot get the c++ code
    # to handle, but the python code seems to do ok.
    # Look for such data returned by the c++ process with an error, and then
    # try to retrieve it again with a python subprocess. This will be slow, but
    # hopefully this is a rare occurrance...
    if use_cpp_subprocess and 'error' in output_dictionary and \
            'item_type' in output_dictionary:
        if output_dictionary['error'] != 'False' and \
                (output_dictionary['item_type'] == 'text-array' or
                 output_dictionary['item_type'] == 'text'):
            if debug:
                print('Found an error in the output!')
                print('  ', output_dictionary['error'])
                print('  Well I\'ll be damned - strange text values!')

            output_dictionary = \
                get_object_as_user(path, username, get_tree_path, True, debug)

    if debug:
        print('')
        print_dictionary_suppress_image(output_dictionary, False)
        print('')
        print('Check who is running this script (should be root):')
        print('  os.getuid(): ', os.getuid())
        print('  os.getgid(): ', os.getgid())
        print('')

        print('*** END resources_py/handle_object.py get_object_as_user ***')

    return output_dictionary


def get_object(input_string, debug):

    """
    Input:
        - folder/file/object path name
        - debug (True or False)

    Output:
        - output_object
        - object_type
        - data_type
        - shape
    """

    if debug:
        print('*** START resources_py/handle_object.py get_object ***')
        print('')
        print('Check who is running this script (should not be root if '
              'running as subprocess):')
        print('  os.getuid():  ', os.getuid())
        print('  os.getgid():  ', os.getgid())
        print('')
        print('  os.geteuid(): ', os.geteuid())
        print('  os.getegid(): ', os.getegid())
        print('')

    # Start profiling
    pr = start_profiling(False)

    output_object = shape = can_user_read_file = bad_pixels_exist = False
    section = bpr_limits = is_downsampled = False

    # Parse input - directory? file? directory in a file? object in a file?
    object_type, file_path, h5_path, slice_par = \
        parse_object_path(input_string, debug)

    # Check if the folder, file, or hdf5 object exists, and what type of hdf5
    # object it is that we might have
    object_exists, data_type, shape = \
        does_object_exist(object_type, file_path, h5_path, debug)

    if object_exists:

        # Check if user executing this process has permissions to view this
        # folder or file
        can_user_read_file = os.access(file_path, os.R_OK)

        if debug:
            print('')
            print('file_path:         ', file_path)
            print('can_user_read_file:', can_user_read_file)
            print('')

    if can_user_read_file:

        # If this is a folder, get the contents
        if object_type == 'folder':

            output_object = get_folder_contents(file_path, debug)

        # If this is a file, get the contents
        if object_type == 'h5_file':

            output_object = \
                get_h5_file_folder_contents(file_path, h5_path, data_type,
                                            debug)

        # If this is an object within an hdf5 file, get it!
        if object_type == 'h5_object':

            # If this is a folder in an hdf5 file, get the contents
            if data_type == 'h5_folder':

                output_object = \
                    get_h5_file_folder_contents(file_path, h5_path, data_type,
                                                debug)

            # If this is a dataset, get the values
            elif data_type == 'image' or data_type == 'image-series' \
                or data_type == 'line' or data_type == 'text' \
                    or data_type == 'number' or data_type == 'text-array':

                output_object, section, bpr_limits, bad_pixels_exist, \
                    is_downsampled = get_dataset(file_path, h5_path, data_type,
                                                 shape, slice_par, debug)

    # Create an output dictionary, save some information
    output_dictionary = \
        convert_data_to_dict_list(input_string, output_object, object_type,
                                  data_type, shape, section, bpr_limits,
                                  bad_pixels_exist, is_downsampled, debug)

    # Stop profiling
    stop_profiling(pr, 'profile of get_object')

    if debug:
        print('Check who is running this script (should not be root if '
              'running as subprocess):')
        print('  os.getuid(): ', os.getuid())
        print('  os.getgid(): ', os.getgid())
        print('')
        print('*** END resources_py/handle_object.py get_object ***')
        print('')

    return output_dictionary


def get_dataset(file_path, h5_path, data_type, shape, slice_par, debug):

    if debug:
        print('')
        print('*** START resources_py/handle_object.py get_dataset ***')
        print('')
        print('data_type: ', data_type)
        print('')

    dataset = {}
    bpr_limits = section = ()
    bad_pixels_exist = False
    is_downsampled = False

    if data_type == 'image' or data_type == 'image-series' \
            or data_type == 'line' or data_type == 'text-array':

        # Get the image or array from the file
        image, section = handle_images.get_image(file_path, h5_path, data_type,
                                                 shape, slice_par, debug)

        # Decimate image if necessary
        #   - Need to add an option which takes into account screen/plot size
        #   - Should also implement this for arrays
        if data_type == 'image' or data_type == 'image-series':

            size_limit = 1.6e5

            dataset, bpr_limits, bad_pixels_exist, is_downsampled = \
                handle_images.decimate_image(image, size_limit, debug)

        else:
            dataset = image

        if data_type == 'text-array':
            dataset = str(dataset[0])

        if debug:
            print('dataset:')
            print(dataset)
            print('')

    if data_type == 'number' or data_type == 'text':

        # Get the value from the file
        dataset = handle_scalars.get_number(file_path, h5_path, False, debug)

        if debug:
            print(dataset)
            print('')

    if debug:
        print('')
        print('*** END resources_py/handle_object.py get_dataset ***')
        print('')

    return dataset, section, bpr_limits, bad_pixels_exist, is_downsampled


def print_dictionary_suppress_image(output_dictionary, print_all):
    """
    Display the output - suppress image output to terminal
    """

    print('')

    if 'values' in output_dictionary and 'shape' in output_dictionary:

        print('{')

        for key, value in output_dictionary.items():
            if str(key) != 'values':
                print('  ' + key + ' : ', value)
            else:
                if not print_all:
                    print('  values : <image --> use -a option to print all>')
                else:
                    print('  values :')
                    print(json.dumps(output_dictionary['values'],
                                     indent=2, sort_keys=True))

        print('}')

    else:
        # print(output_dictionary)
        print(json.dumps(output_dictionary, indent=2, sort_keys=True))

    print('')


########
# MAIN #
########

def main(argv):
    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description='Reduce (downsample) an image contained in an hdf5 file')
    parser.add_argument("input_string", nargs=1,
                        help='The input hdf5 file name')
    parser.add_argument("-u", '--username', required=False,
                        default='jasbru', help='the username of the ... user')
    parser.add_argument("-uid", '--uid_number', required=False,
                        default=False, help='the uid number of the user')
    parser.add_argument("-gid", '--gid_number', required=False,
                        default=False,
                        help='the gid number of the user')
    parser.add_argument("-gids", '--gid_numbers', required=False,
                        default=False,
                        help='a list of gid numbers of the user')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='debug output')
    parser.add_argument('-f', '--force_no_cpp', action='store_true',
                        help='force the use of the python subprocess')
    parser.add_argument('-t', '--get_tree_path', action='store_true',
                        help='get folder contents for all ancestors of object')
    parser.add_argument('-s', '--string_output', action='store_true',
                        help='output in one big string')
    parser.add_argument('-r', '--run_subprocess_as_user', action='store_true',
                        help='run sub process as user')
    parser.add_argument('-a', '--print_all', action='store_true',
                        help='print image output, suppressed by default')

    # Print a little extra in addition to the standard help message
    if len(argv) == 0 or '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print('  python resources_py/handle_object.py '
                  'data_dir/ ')
            print('  python resources_py/handle_object.py '
                  'data_dir/biomax/example-data.h5')
            print('  python resources_py/handle_object.py '
                  'data_dir/biomax/example-data.h5/scan_1/data_1/image')
            print('  python resources_py/handle_object.py '
                  'data_dir/biomax/example-data.h5/scan_1/data_1/image'
                  '[1:10,1:20]')
            print('  python resources_py/handle_object.py '
                  'data_dir/ -u jasbru -uid 1000 -gid 1000 -gids '
                  '"[999, 115, 1000]" -s')
            print('')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print(args)

    if args.run_subprocess_as_user:

        # Get the object as the given user (assuming the user executing this
        # script is root or some other user with sufficient privileges)
        output_dictionary = get_object_as_user(args.input_string[0],
                                               args.username,
                                               args.get_tree_path,
                                               args.force_no_cpp,
                                               args.debug)

        print('')
        print('OUTPUT_DICTIONARY received from subprocess:')
        print_dictionary_suppress_image(output_dictionary, args.print_all)
        print('')

    else:

        # Get the object
        output_dictionary = get_object(args.input_string[0], args.debug)

        # Print simple output - good for returning to other processes.
        if args.string_output:

            # Keywords are suppllied before and after the desired output to
            # help with parsing by other applications.
            print('')
            print('OUTPUT_DICTIONARY')
            print(output_dictionary)
            print('OUTPUT_DICTIONARY')
            print('')

        # Print a bit nicer output for running in a terminal
        else:

            print_dictionary_suppress_image(output_dictionary, args.print_all)


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':

    # Local resources
    import handle_images
    import handle_scalars
    import get_user_info

    main(sys.argv[1:])

else:

    # Local resources
    from resources_py import handle_images
    from resources_py import handle_scalars
    from resources_py import get_user_info
