#!/usr/bin/env python

'''
A (hopefully) simple REST interface to hdf5 files
CAS authentication required to view data
'''

#####################
# IMPORT LIBRARIES ##
#####################

# General systems
import os
import sys
import argparse
import logging
from logging.handlers import RotatingFileHandler

# Flask
from flask import Flask, jsonify, request, after_this_request, redirect, \
    session, escape

# CORS
from flask_cors import cross_origin

# CAS authentication
import urllib

# json
import json

# gzipping output
import gzip
import functools
from io import StringIO as IO

# Local resources
from resources_py import handle_object

# favicon
from flask import send_from_directory


######################
# CREATE APPLICATION #
######################

app = Flask(__name__)

# Session related variables
app.SECRET_KEY = 'blahblahblah'
app.SESSION_COOKIE_SECURE = True

# Some global variables
APP_DIR = ''


###########
# HELPERS #
###########

def setup_logger(debug):

    """
    Setup up a simple log file
    """

    formatter = logging.Formatter(
        "[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
    log_file_path = os.path.join(APP_DIR, 'log/hdf5-simple-server.log')
    handler = RotatingFileHandler(log_file_path, maxBytes=10000000,
                                  backupCount=5)
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)

    app.logger.addHandler(handler)
    if debug:
        app.logger.setLevel(logging.DEBUG)
    else:
        app.logger.setLevel(logging.INFO)


def gzipped(f):

    """
    A decorator function for compressing output - should test more thoroughly
    Grabbed from http://flask.pocoo.org/snippets/122/
    """

    @functools.wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')

            if 'gzip' not in accept_encoding.lower():
                return response

            response.direct_passthrough = False

            if (response.status_code < 200 or response.status_code >= 300 or
                    'Content-Encoding' in response.headers):
                return response

            gzip_buffer = IO()
            gzip_file = gzip.GzipFile(mode='wb', fileobj=gzip_buffer)
            gzip_file.write(response.data)
            gzip_file.close()

            response.data = gzip_buffer.getvalue()
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)

            return response

        return f(*args, **kwargs)

    return view_func


@app.route('/sessionstatus/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def sessionstatus():

    """ Check who is logged in """

    output_dictionary = {}
    output_dictionary['sessionStatus'] = False
    output_dictionary['userName'] = ""

    if 'userName' in session:
        print(session['userName']['uid'])

        output_dictionary['sessionStatus'] = True

        userName = session['userName']['uid']
        firstName = session['userName']['firstName']

        if not isinstance(userName, str):
            userName = userName.decode('utf8').replace("'", '"')
        if not isinstance(firstName, str):
            firstName = firstName.decode('utf8').replace("'", '"')

        output_dictionary['userName'] = userName
        output_dictionary['firstName'] = firstName

        print('Logged in as %s' % escape(output_dictionary['userName']))
    else:
        print('No user currently logged in')

    print(json.dumps(output_dictionary, indent=2, sort_keys=True))

    return jsonify(output_dictionary)


@app.route('/helloworld/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def helloworld():

    """
        For debugging.
        Simple 'hello world'
    """

    output_dictionary = {}
    output_dictionary['0'] = "Hello World!"
    output_dictionary['1'] = [0, 1, 2, 3]

    print(json.dumps(output_dictionary, indent=2, sort_keys=True))

    return jsonify(output_dictionary)


####################
# PUBLIC ENDPOINTS #
####################

@app.route('/logout/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def logout():

    """
    Delete session and logout from CAS
    """

    app.logger.info('/logout/ called')

    session.pop('userName', None)

    return redirect(app.config['CAS_SERVER'] + app.config['CAS_LOGOUT_ROUTE'])


@app.route('/validatecas', methods=['GET'])
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def validateTicket():

    """
    Validate a CAS ticket
    """

    app.logger.info('/validate/ called')

    # The output dictionary
    output_dictionary = {}
    output_dictionary['validated'] = False

    # The CAS server's validation url
    validation_url = app.config['CAS_SERVER'] + \
        app.config['CAS_VALIDATE_ROUTE']

    ticket = request.args.get('ticket')
    service = request.args.get('service')

    # Create the full validation url, including the ticket and application url
    params = {'ticket': ticket, 'service': service}
    url = '%s%s?%s' % (app.config['CAS_SERVER'],
                       app.config['CAS_VALIDATE_ROUTE'],
                       urllib.parse.urlencode(params))

    app.logger.debug('ticket:  ' + str(ticket))
    app.logger.debug('service: ' + str(service))
    app.logger.debug('validation_url: ' + str(validation_url))
    app.logger.debug('complete validation url: ' + str(url))

    # Contact the CAS server
    page = urllib.request.urlopen(url)

    if 'userName' in session:
        app.logger.debug('Logged in as: ' + str(session['userName']))
    else:
        app.logger.debug('No user currently logged in')

    try:
        user = None
        attributes = {}

        # Read the response from the CAS server
        response = page.read()

        app.logger.debug('validation request response: ' + str(response))

        # Get information from the response, save to a dictionary
        try:
            from xml.etree import ElementTree
        except ImportError:
            from elementtree import ElementTree

        tree = ElementTree.fromstring(response)

        if tree[0].tag.endswith('authenticationSuccess'):
            for element in tree[0]:
                if element.tag.endswith('user'):
                    user = element.text
                elif element.tag.endswith('attributes'):
                    for attribute in element:
                        attributes[attribute.tag.split("}").pop()] = \
                            attribute.text

        app.logger.debug('user: ' + str(user))
        app.logger.debug('attributes: ' + str(attributes))

        # Save a few things to the output dictionary that are to be returned
        # to the web application
        output_dictionary['validated'] = True
        output_dictionary['userName'] = user
        if 'givenName' in attributes:
            output_dictionary['firstName'] = attributes['givenName']

        # Save the user name to a session dictionary
        session['userName'] = {'uid': user,
                               'firstName': output_dictionary['firstName']}
        session.modified = True

    finally:
        page.close()

    # Convert the output to json and return
    output = jsonify(output_dictionary)

    return output


@app.route('/validatejwt', methods=['GET'])
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def validateJWT():

    """
    Validate a CAS ticket
    """

    app.logger.info('/validatejwt/ called')

    # The output dictionary
    output_dictionary = {}
    output_dictionary['validated'] = False

    tokenvalue = request.args.get('tokenvalue')
    ticket = request.args.get('ticket')
    service = request.args.get('service')

    app.logger.debug('tokenvalue:  ' + str(tokenvalue))
    app.logger.debug('ticket:  ' + str(ticket))
    app.logger.debug('service: ' + str(service))

    ######################################################################
    # Attempting to verify token via post request - no luck!
    ######################################################################
    # import requests

    # url = 'https://auth.maxiv.lu.se/v1/decode'
    # myobj = {'maxiv-jwt-auth': ticket}
    # headers = {"Accept": "application/json",
    #            "Content-Type": "application/json"}
    # response = requests.post(url, json=myobj, headers=headers, timeout=5.0)

    # app.logger.debug('response: ' + str(response))
    ######################################################################

    tokendict = json.loads(tokenvalue)
    if "username" in tokendict:
        username = str(tokendict["username"].lower())
        firstname = str(tokendict["name"]).split(" ")[0]
        app.logger.debug('username:  ' + username)
        app.logger.debug('firstname:  ' + firstname)

        output_dictionary['validated'] = True
        output_dictionary['userName'] = username
        output_dictionary['firstName'] = firstname

        session['userName'] = {'uid': username, 'firstName': firstname}
        session.modified = True

    # Convert the output to json and return
    output = jsonify(output_dictionary)

    return output


@app.route('/setupdemo/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def setupDemo():

    """
    setup demo version
    """

    app.logger.info('/setupdemo/ called')

    # The output dictionary
    output_dictionary = {}
    output_dictionary['validated'] = False

    try:
        user = None

        user = app.config['DEMO_USER']
        run_demo = app.config['RUN_DEMO']

        app.logger.debug('user: ' + str(user))

        # Save a few things to the output dictionary that are to be returned
        # to the web application
        if run_demo:
            output_dictionary['validated'] = True
            output_dictionary['userName'] = user
            output_dictionary['firstName'] = 'To The Demo'

            # Save the user name to a session dictionary
            session['userName'] = {'uid': user,
                                   'firstName': output_dictionary['firstName']}
            session.modified = True
        else:
            output_dictionary['validated'] = False

    finally:
        print('demo time!')

    # Convert the output to json and return
    output = jsonify(output_dictionary)

    return output


@app.route('/usejwt/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def usejwt():

    """
    Check to see if CAS authentication has been configured
    """

    app.logger.info('/usejwt/ called')

    output_dictionary = {}
    output_dictionary['jwtServer'] = app.config['JWT_SERVER']
    output_dictionary['runDemo'] = app.config['RUN_DEMO']

    if app.debug:
        print(json.dumps(output_dictionary, indent=2, sort_keys=True))

    return jsonify(output_dictionary)


@app.route('/usecas/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def usecas():

    """
    Check to see if CAS authentication has been configured
    """

    app.logger.info('/usecas/ called')

    output_dictionary = {}
    output_dictionary['casServer'] = app.config['CAS_SERVER']
    output_dictionary['runDemo'] = app.config['RUN_DEMO']

    if app.debug:
        print(json.dumps(output_dictionary, indent=2, sort_keys=True))

    return jsonify(output_dictionary)


@app.route('/favicon.ico/')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def favicon():

    """
    Serve the favicon, stop browser debuggers from complaining
    """

    app.logger.debug('/favicon.ico/ called')

    return send_from_directory(APP_DIR + '/static/', 'favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


# @gzipped
@app.route('/')
@app.route('/<path:path>')
@cross_origin(headers=['Content-Type'], supports_credentials=[True])
def get_data(path=''):

    """
    The main function - it's used for getting lists of directory and file
    contents as well as dataset contents.

    Submitted path names are intended to be as simple as possible, eg:
        - Get everything in the base data directory (as defined in config.cfg):
            /

        - List the contents of a sub-directory:
            /biomax/

        - List the contents of an hdf5 file that is located in a sub-directory:
            /biomax/example-data.h5

        - Get dataset values and information about it:
            /biomax/example-data.h5/scan_1/data_1/image

    If the argument "treepath" is present, then the folder contents of all of
    the ancestors of the given data path are returned, not the data itself.
    """

    app.logger.info('/get_data called')
    app.logger.debug('input path: [' + str(path) + ']')

    # Check for a url parameter
    get_tree_path = request.args.get('treepath')
    app.logger.debug('get tree path: [' + str(get_tree_path) + ']')
    if get_tree_path:
        get_tree_path = True
    else:
        get_tree_path = False
    app.logger.debug('get tree path: [' + str(get_tree_path) + ']')

    # The output variable for this function
    output_dictionary = {}

    # See if there is a user session
    uid = ''
    if 'userName' in session:

        app.logger.info('Session : logged in as: ' +
                        str(session['userName']['uid']))

        # Get the username from the session, which is created during CAS
        # ticket verification
        uid = session['userName']['uid']

        # Get the object - folder contents, file contents, dataset, etc. - as
        # the CAS logged-in user
        output_dictionary = \
            handle_object.get_object_as_user(path, uid, get_tree_path, False,
                                             app.debug)

    else:
        app.logger.info('Session : No user logged in')
        output_dictionary['sessionStatus'] = 'not_logged_in'

    # Convert the dictionary to json format and return
    output_json = jsonify(output_dictionary)

    return output_json


########
# MAIN #
########

def main(argv):
    '''
    The main function - usage and help, argument parsing
    '''

    # Setup options
    parser = argparse.ArgumentParser(
        description=('Start a flask server which makes available '
                     ' data from hdf5 files in json format.'))
    parser.add_argument('-d', '--debug', action='store_true',
                        help="Debug output")
    parser.add_argument("-c", '--config_file', required=False,
                        default='config/config.cfg',
                        help='the hdf5 file containing the mask')

    # Print a little extra in addition to the standard help message
    if '-h' in argv or '--help' in argv:
        try:
            args = parser.parse_args(['-h'])
        except SystemExit:
            print('')
            print('Examples of usage:')
            print('')
            print('  Start the application in debug mode:')
            print('    python app.py -d')
            print('')
            print('View the output:')
            print('  curl -k https://w-jasbru-pc-0.maxiv.lu.se:9443/')
            print('  curl -k https://w-jasbru-pc-0.maxiv.lu.se:9443/'
                  'biomax/example-data.h5/scan_1/data_1/image')
            print('')
            sys.exit()
    else:
        args = parser.parse_args(argv)

    if args.debug:
        print('')
        print(args)

    # Note location of app.py file, as resources are located relative to it
    global APP_DIR
    APP_DIR = os.path.dirname(os.path.abspath(__file__))
    print('APP_DIR:', APP_DIR)

    # Start writing stuff to the logs
    setup_logger(args.debug)

    # Start the server
    try:

        # Load configuration file
        config_path = os.path.join(APP_DIR + '/' + args.config_file)
        print('config_path:', config_path)
        app.config.from_pyfile(config_path)
        handle_object.CONFIG_PATH = config_path

        # Engage!
        if app.config['PROTOCOL'] == 'https':

            print('running with ssl')

            # Setup SSL certificates
            ssl_crt_path = os.path.join(APP_DIR, app.config['SSL_CERT'])
            ssl_key_path = os.path.join(APP_DIR, app.config['SSL_KEY'])
            context = (ssl_crt_path, ssl_key_path)

            app.run(host='0.0.0.0', port=app.config['PORT'], debug=args.debug,
                    use_reloader=args.debug, threaded=True,
                    ssl_context=context)
        else:

            print('running without ssl')

            app.run(host='0.0.0.0', port=app.config['PORT'], debug=args.debug,
                    use_reloader=args.debug, threaded=True)

    # Quit nicely with ctrl-c input
    except KeyboardInterrupt:
        app.logger.info('Exiting flask server')
        print('Exiting flask server')
        sys.exit()


#######################
# RUN THE APPLICATION #
#######################

if __name__ == '__main__':
    main(sys.argv[1:])
