# HDF5 SIMPLE SERVER

1. [Quick Restart Instructions For MAX IV](#quick-restart-instructions)
2. [Overview](#overview)
3. [Installation](#installation)
    1. [BUILD & RUN - DEMO MODE](#build-run-demo-mode)
    2. [BUILD & RUN - CAS & HTTPS MODE](#build-run-cas-https-mode)
    3. [RUN DEVELOPMENT MODE](#run-development-mode)
4. [Demo Server](#demo-server)


## MAX IV

### QUICK RESTART INSTRUCTIONS

In case the VM on which this has been installed was rebooted, the
back-end server may need to be restarted, and the front-end may need to be
restarted too.  This will need to be done with the service account "gitlab."

1. Restart back-end:
```bash
ssh w-v-hdf5view-0
  sudo su - gitlab
  cd /var/www/hdf5-viewer/hdf5-simple-server/
  make stop
  make start-cas-https-maxiv
```

It could happen that an error occurs at this point concerning "cgroup
mountpoints."  This has to do with a docker error and can be fixed with:
```bash
  make cgroup-error-fix
```

2. Restart front-end, still as the "gitlab" user:
```bash
  cd /var/www/hdf5-viewer/hdf5-web-gui/
  make stop
  make start-cas-https-maxiv
```

For both parts of the application, one can watch the logs:
```bash
  make logs
```

And check that the application works:
  [HDF5 Viewer](https://hdf5view.maxiv.lu.se/)

### CI
The continuous integration (CI) pipeline is only setup to work inside of the
MAX IV network at present.

When changes to particular files on the master branch are made and pushed to
the repository, the CI pipeline will be triggered, which will result in:
1. A new docker image being built and pushed to two registries:
    1. An internal MAX IV registry
    2. A public [gitlab.com registry](https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-simple-server/container_registry)
2. The new image being pulled to two servers:
    1. The internal, production HDF5 Viewer server
    2. The publicly available, [Demo HDF5 View Server](http://demo.maxiv.lu.se/)
3. The docker container will be restarted on both servers

For more, see the CI configuration: [.gitlab-ci.yml](https://gitlab.maxiv.lu.se/scisw/hdf5-viewer/hdf5-simple-server/-/blob/master/.gitlab-ci.yml)


## OVERVIEW

This is meant to be a simple server which is to serve as the backend
for the HDF5 Web GUI, or perhaps other applications.

Interaction with the server is via a REST API, written in python 3.8, HDF5
files are examined in C++ making use of h5cpp wrapper, then responses are
returned in the form of json objects.

This server makes use of a number of libraries, including:
* [h5py](https://github.com/h5py/h5py)
* [numpy](https://github.com/numpy/numpy)
* [flask](https://github.com/pallets/flask)
* [scikit-image](https://github.com/scikit-image/scikit-image)
* [hdf5plugin](https://github.com/dectris/HDF5Plugin)
* [h5cpp wrapper](https://github.com/ess-dmsc/h5cpp)

![screenshot](static/screenshot.png)

## INSTALLATION

Installing this server application can be done in two different ways:
1. Using the supplied docker configuration files
2. As a system service

When using either method, this hdf5-simple-server can be on the same machine
where hdf5-web-gui is installed, or they can be on seperate machines.


## INSTALLATION WITH DOCKER

### GIT THE CODE

First thing to do is download the code.

Git code from the private MAX IV repository:
```bash
git clone git@gitlab.maxiv.lu.se:scisw/hdf5-viewer/hdf5-simple-server.git
```

Or from the public gitlab repository:
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-simple-server.git
```


### BUILD & RUN - DEMO MODE

The easiest thing to to first is to run in the so-called demo mode, which here
means that the application is run with http (therefore no ssl certificates
needed) and no authentication is needed - everything is run as the root user
within a docker container.

The only dependencies in this case are docker, docker-compose, and make.

There is a Makefile present which is intended to simplify the building and
running of the docker images. For usage information:
```bash
make
```

Assuming this server repository is to be run as a back-end to the hdf5-web-gui,
it can be nice to start them and install them at the same time, and monitor
their logs and stop and start them as needed.

So, using two separate terminals:

#### Terminal for hdf5-simple-server
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-simple-server.git
cd hdf5-simple-server/
make build
make tag-stable
make start-demo
make logs
```
At the end of the image build, which should take around 10 minutes, a message
should appear like the following, but with a different tag:

*Successfully tagged docker.maxiv.lu.se/maxiv-scisw-hdf5/hdf5-simple-server:2021-04-13_6945255*

Logs should display some output.


#### Check Server Output
Now check the server output in a web browser, it should look like the
screenshot below:

[http://localhost:8081](http://localhost:8081)

![screenshot folder](static/screenshot_example_output_demo_folder.png)


#### Terminal for hdf5-web-gui
```bash
git clone https://gitlab.com/MAXIV-SCISW/HDF5-VIEWER/hdf5-web-gui.git
cd hdf5-web-gui/
make build
make tag-stable
make start-demo
make logs
```
At the end of the image build, which should take around 1 minute, a message
should appear:

*Successfully tagged maxiv-scisw-hdf5/hdf5-web-gui:local*

Logs will start to appear when url accessed.


#### Check GUI Output
Then hopefully the front-end will also be working, and is successfully
communicating with the server:

[http://localhost:8082/](http://localhost:8082/)

![screenshot](static/screenshot_web_gui.png)


#### Stopping the applications:
```bash
make stop
```

And that's it for running in demo mode!

### BUILD & RUN - CAS & HTTPS MODE

This should (hopefully) be just as easy to run as the demo mode - once the
various configurations are setup correctly.

Also, authentication is done via CAS (something else could be used too) and
the host system needs to have ssl certificates installed.

The docker image used here is the same as the one used in the demo-mode, just
the configurations are different.  So if you haven't done so, build the image:
```bash
make build
make tag-stable
```

Now, a few configuration files will need to be edited, most likely just on the
lines mentioned below, where various server names will need to be changed,
the data directory set, and ssl certificate locations set:
```bash
vim config/config-cas-https.cfg
    HOST_NAME='jasbru.maxiv.lu.se'
    CAS_SERVER='https://cas.maxiv.lu.se'

vim docker/docker-compose-cas-https.yml
    # Data directory
      - /data/:/app/data_dir:ro
    # The ssl certificates for the hdf5 server
      - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.crt:/app/certs/ssl_cert.crt:ro
      - /etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.key:/app/certs/ssl_cert.key:ro
    # Set the host name - needs to match certificate and config file contents
      hostname: jasbru.maxiv.lu.se
```

Then, start the docker container, using the CAS & HTTPS mode configuration
files:
```bash
make start-cas-https
```

And watch the logs:
```bash
make logs
--> ctrl-c to exit
```

And then, if you also get hdf5-web-gui running in CAS & HTTPS mode, then the
web application will be availble in a web browser here:

[https://my-host-name:8843](https://my-host-name:8843)


Then to stop the application:
```bash
make stop
```

Now, the user information is accessed in a hack-ish way via two files which
are created with the 'make start-cas-https' command:
```bash
user-info/
├── group
└── passwd
```

This file needs to be updated on systems which might have new users added
regularly.  To accomplish this, there is a cron job which should be installed,
perhaps setup to run hourly:
```bash
sudo cp config/hdf5-simple-server_update-user-info /etc/cron.hourly/.
sudo chmod 755 /etc/cron.hourly/hdf5-simple-server_update-user-info
```
Check the path used in the cron job, and change if needed.


## DEVELOPMENT INSTALLATION
The development installation is meant to be run on a development machine
without using docker, just a bare installation.

With this method, many dependencies will need to be installed, which for the
most part have been placed within makefile targets.

The system dependencies have been succefully installed on a debian machine, and
the makefile is not yet working 100% for installing on a centOS machine, so
those dependencies will need to be installed by hand.  See the makefile target
"install-dep-centos" and try to install the packages. Sorry centos users!

### INSTALL EVERYTHING
Several steps here require sudo access.

On a debian machine:
```bash
make install-dep-debian
make install-h5cpp
make compile-cpp-code
make venv
```

Or on a centOS machine (not successfully tested!):
```bash
make install-dep-centos
make install-h5cpp
make compile-cpp-code
make venv
```

### RUN DEVELOPMENT MODE
This will require sudo access, and the server process will be run as root.

There are two development mode versions to use, with and without https & user
authenticaiton.

#### RUN DEVELOPMENT MODE - DEMO
Configure the port to be used and the location of the data direcotry.  In this
example, I am using the data directory that comes with the repository, which
has been installed in '/var/www/hdf5-viewer/' - change this to where you have
it installed:
```bash
vim config/config-dev-demo.cfg
    PORT=8081
    DATA_DIR='/var/www/hdf5-viewer/hdf5-simple-server/data_dir/demo-data/'
```

Now start the server, output will be displayed in the terminal:
```bash
make start-dev-demo
```

Ignoring the use of any GUI front-end for the moment, the output from the
server can now be checked in a web browser:

[http://localhost:8081/](http://localhost:8081/)

![screenshot folder](static/screenshot_example_output_folder.png)

[http://localhost:8081/var/www/hdf5-viewer/hdf5-simple-server/data_dir/demo-data/example-data.h5](http://localhost:8081/var/www/hdf5-viewer/hdf5-simple-server/data_dir/demo-data/example-data.h5)

![screenshot number](static/screenshot_example_output_number.png)

Careful with large images in development mode, as all elements will be printed
to the terminal!

#### RUN DEVELOPMENT MODE - CAS & HTTPS
This is pretty much the same as the above demo mode, but now the location
of ssl certificates must be specified, along with the use of https, the
server name, port, data directory location, CAS server, CAS routes, and
the location of the HDF5 plugins:
```bash
vim config/config-dev-cas-https.cfg
    HOST_NAME='jasbru.maxiv.lu.se'
    PROTOCOL='https'
    PORT=8081
    DATA_DIR='/var/www/hdf5-example-data/'
    SSL_CERT='/etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.crt'
    SSL_KEY='/etc/ssl/certs/w-jasbru-pc-0_maxiv_lu_se.key'
    RUN_DEMO='false'
    CAS_SERVER='https://cas.maxiv.lu.se'
    CAS_LOGIN_ROUTE='/cas/login'
    CAS_LOGOUT_ROUTE='/cas/logout'
    CAS_VALIDATE_ROUTE='/cas/p3/serviceValidate'
    HDF5_PLUGIN_PATH='/var/www/hdf5-viewer/hdf5-simple-server/.env/lib/python3.8/site-packages/hdf5plugin/plugins'

```

Now start the server, output will be displayed in the terminal:
```bash
make start-dev-cas-https
```

![screenshot_server_output_debug](static/screenshot_server_output_debug.png)

And look at the output in a web browser:

[https://jasbru.maxiv.lu.se:8081/](https://jasbru.maxiv.lu.se:8081/)

![screenshot_server_output_data_dir](static/screenshot_server_output_data_dir.png)
