# Compile

Use the convenient compile script:
```bash
cd /var/www/hdf5-simple-server/resources_cpp/
./compile.sh
```

Which simply does:
```bash
rm -rf bin/ build/
cmake -DCMAKE_BUILD_TYPE=Release -DCONAN=DISABLE -B build .
cmake --build build/.
```

The executable will then appear here:
```bash
/var/www/hdf5-simple-server/resources_cpp/bin/handle_object
```

# Run
From a terminal:
```bash
cd /var/www/hdf5-simple-server/resources_cpp/
bin/handle_object /var/www/hdf5-example-data/amazing-data/example-data.h5 scan_1/data_1/image
```

# Code Structure
The code is organized as follows:
```bash
├── CMakeLists.txt
├── include/
│   ├── handle_filesystem.hpp
│   └── handle_image.hpp
├── README.md
└── src/
    ├── handle_filesystem.cpp
    ├── handle_image.cpp
    └── handle_object.cpp
```
where handle_object.cpp contains the main() function.

After running cmake and compiling, a build directory will be created, along with
a bin directory which contains the executable:
```bash
├── build/
└── bin/
    └── handle_object
```
