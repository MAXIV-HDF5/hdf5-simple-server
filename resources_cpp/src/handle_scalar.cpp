// SOURCE FILES
#include "handle_scalar.hpp"


// Get dataset values of the scalar type - includes single numbers and strings
getScalarOutput getScalar(hdf5::node::Dataset Dataset, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "*** START getScalar ***" << endl;
    }

    // Output data
    string string_value = "";
    string object_type = "h5_unknown";
    string error = "False";

    // Intermediary variables
    float float_value;
    int int_value;

    auto DataType = Dataset.datatype().get_class();
    if (debug) {
        cout << "  DataType:      " << DataType << endl;
    }

    try {
        if (DataType == hdf5::datatype::Class(H5T_STRING)) {
            object_type = "text";
            Dataset.read(string_value);
        }

        if (DataType == hdf5::datatype::Class(H5T_FLOAT)) {
            object_type = "number";
            Dataset.read(float_value);
            string_value = std::to_string(float_value);
        }

        if (DataType == hdf5::datatype::Class(H5T_INTEGER)) {
            object_type = "number";
            Dataset.read(int_value);
            string_value = std::to_string(int_value);
        }
    } catch (std::bad_alloc) {

        error = "Could not read scalar dataset";

        if (debug) {
            cout << endl;
            cout << "  **********************************************" << endl;
            cout << "  *** Problem occured reading scalar dataset ***" << endl;
            cout << "  **********************************************" << endl;
            cout << endl;
        }
    }

    if (debug) {
        cout << "  object_type:   " << object_type << endl;
        cout << "  string_value:  " << string_value << endl;
        cout << "  error:         " << error << endl;
        cout << "***  END  getScalar ***" << endl;
        cout << endl;
    }

    return {string_value, object_type, error};
}
