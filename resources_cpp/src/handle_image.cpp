// SOURCE FILES
#include "handle_image.hpp"

// Check that the given slice paramters are good, modify them if out of range
verifySliceParamtersOutput verifySliceParamters(vector<int> sliceParametersIn,
    vector <int> shape, bool debug)
{

    if (debug) {
        cout << endl;
        cout << "*** START verifySliceParamters ***" << endl;
        for (int i = 0; i < sliceParametersIn.size(); i++) {
            cout << "  sliceParametersIn[" << i << "]: ";
            cout << sliceParametersIn[i] << endl;
        }
        cout << endl;
    }

    // Outout variables
    bool goodSliceDef = false;
    vector <long unsigned int> sliceParametersOut;

    // Set the absolute limits for image section selection (slicing)
    vector< vector <int> > limits;
    for (int i = 0; i < shape.size(); i++) {
        limits.push_back({0, shape[i]});
        limits.push_back({0, shape[i]});
    }

    // Check for bad slice definitions
    if (sliceParametersIn.size() > 0) {
        goodSliceDef = true;

        if (debug) {
            cout << "*  sliceParametersIn.size(): ";
            cout << sliceParametersIn.size() << endl;
            cout << "*  limits.size(): " << limits.size() << endl;
            cout << endl;
        }

        // The number of slice parameters should match the number of limits
        // parameters, and therefore correspond to the proper dimensions of
        // the object, e.g. an image should have 2 dimensions, 4 slice
        // parameters and 4 limit parameters
        if (sliceParametersIn.size() != limits.size()) {
            goodSliceDef = false;
        }

        // Look at each slice paramter, make sure they are within the proper
        // ranges as defined by the variable limits
        if (goodSliceDef) {
            for (int i = 0; i < sliceParametersIn.size(); i++) {

                if (debug) {
                    cout << "*  sliceParametersIn[" << i << "]: ";
                    cout << sliceParametersIn[i] << endl;
                    cout << "     limits[" << i << "][" << 0 << "]: ";
                    cout << limits[i][0] << endl;
                    cout << "     limits[" << i << "][" << 1 << "]: ";
                    cout << limits[i][1] << endl;
                }

                // If the given slice parameters are out of range, place them
                // at the limits
                if (sliceParametersIn[i] < limits[i][0]) {
                    sliceParametersOut.push_back(limits[i][0]);
                } else if (sliceParametersIn[i] > limits[i][1]) {
                    sliceParametersOut.push_back(limits[i][1]);
                } else {
                    sliceParametersOut.push_back(sliceParametersIn[i]);
                }

                if (i % 2 != 0) {
                    //  If the second element of a pair of slice parameters is
                    //  less than the first element, exchange the parameter.
                    if (sliceParametersOut[i] < sliceParametersOut[i - 1]) {
                        int temp = sliceParametersOut[i];
                        sliceParametersOut[i] = sliceParametersOut[i - 1];
                        sliceParametersOut[i - 1] = temp;
                    }

                    //  If the second element of a pair of slice parameters is
                    //  less equal to the first element, just give up and stop
                    //  using the slice parameters.
                    if (sliceParametersOut[i] == sliceParametersOut[i - 1]) {
                        goodSliceDef = false;
                    }
                }
            }
        }
    }

    // If the given slice paramaters were not well defined, create some which
    // cover the entire image - used by later functions
    if (!goodSliceDef) {
        sliceParametersOut.clear();
        for (int i = 0; i < shape.size(); i++) {
            sliceParametersOut.push_back(0);
            sliceParametersOut.push_back(shape[i]);
        }
    }

    if (debug) {
        cout << endl;
        cout << "Done verifying slice definitions. Results: " << endl;
        cout << "  goodSliceDef: " << goodSliceDef << endl;
        for (int i = 0; i < sliceParametersOut.size(); i++) {
            cout << "  sliceParametersOut[" << i << "]: ";
            cout << sliceParametersOut[i] << endl;
        }
        cout << endl;
        cout << "***  END  verifySliceParamters ***" << endl;
        cout << endl;
    }

    return {goodSliceDef, sliceParametersOut};
}


// Given an hdf5 file object and path name with that hdf5 file open:
//   - a line
//   - an image
//   - one image from an image series
// Images are decimated if necessary.
//   - should do this for arrays too I suppose
getImageOutput getImage(hdf5::node::Dataset Dataset, vector<int> slice_par,
    int maxImageSize, bool debug)
{

    /* TODO
     *   - clean up the code...
     *   - decimate big arrays
     */

    if (debug) {
        cout << endl;
        cout << "*** START getImage ***" << endl;
        cout << "maxImageSize: " << maxImageSize << endl;
        cout << endl;
    }

    // Output data
    vector< vector <float> > image;
    vector< vector <float> > image_ds;
    vector <int> shape;
    string text_out;
    string float_out;
    string object_type = "";
    vector <float> bpr_limits;
    vector <int> section;
    bool bad_pixels_exist = false;
    bool is_downsampled = false;
    string error = "False";

    // Images, image-series, and arrays are all of the dataspace::Simple type
    dataspace::Simple Dataspace(Dataset.dataspace());

    // Information about the dataset
    auto Dimensions = Dataspace.current_dimensions();
    auto MaxDimensions = Dataspace.maximum_dimensions();
    for (int i = 0; i < Dimensions.size(); i++) {
        shape.push_back((const int)(Dimensions[i]));
    }
    if (shape.size() == 1) {
        object_type = "line";
    }
    if (shape.size() == 2) {
        object_type = "image";
    }
    if (shape.size() == 3) {
        object_type = "image-series";
    }

    auto DataTypeClass = Dataset.datatype().get_class();
    auto IsString = (DataTypeClass == hdf5::datatype::Class(H5T_STRING));

    if (debug) {
        cout << "Dataset dimensions" << endl;
        cout << "     Current | Max" << endl;
        for (int i = 0; i < Dimensions.size(); i++) {
            cout << "i: " << i << "       " << Dimensions[i] << " | "
                << MaxDimensions[i] << endl;
        }

        auto Int32Type = datatype::create<std::int32_t>();
        auto UInt32Type = datatype::create<std::uint32_t>();
        auto FloatType = datatype::create<float>();
        auto CurrentType = Dataset.datatype();
        auto DatasetType = Dataset.type();
        auto DataspaceType = Dataspace.type();
        auto IsValid = Dataspace.is_valid();
        auto DataspaceSize = Dataspace.size();

        cout << "\nData type\n";
        cout << "Is:            " << DataTypeClass << endl;
        cout << "Is  int32:     " << (Int32Type == CurrentType) << endl;
        cout << "Is uint32:     " << (UInt32Type == CurrentType) << endl;
        cout << "Is  float:     " << (FloatType == CurrentType) << endl;
        cout << "Is string:     " << IsString << endl;
        cout << "DatasetType:   " << DatasetType << endl;
        cout << "DataspaceType: " << DataspaceType << endl;
        cout << "IsValid:       " << IsValid << endl;
        cout << "DataspaceSize: " << DataspaceSize << endl;
        cout << "object_type:   " << object_type << endl;
    }

    // Check if the given slice parameters are good or not.
    // If not, they will be set to cover the entire image.
    verifySliceParamtersOutput sliceParCheck = verifySliceParamters(slice_par,
        shape, debug);
    vector <long unsigned int> slicePar = sliceParCheck.sliceParametersOut;

    // Parameters used for defining a hyperslab
    long unsigned int slabOffsetRow;
    long unsigned int slabOffsetCol;
    long unsigned int slabDimRow;
    long unsigned int slabDimCol;
    hdf5::Dimensions offset;
    hdf5::Dimensions block;

    // Diferent types of datasets require different numbers of offset and block
    // parameters
    if (object_type == "line") {
        // Convert slice notation to the offset and block notation of hdf5
        slabOffsetCol = slicePar[0];
        slabDimRow = 1;
        slabDimCol = slicePar[1] - slicePar[0];

        // Define the hyperslab offset and slab dimensions
        offset.push_back(slabOffsetCol);
        block.push_back(slabDimCol);
    }

    if (object_type == "image") {
        // Convert slice notation to the offset and block notation of hdf5
        slabOffsetRow = slicePar[0];
        slabOffsetCol = slicePar[2];
        slabDimRow = slicePar[1] - slicePar[0];
        slabDimCol = slicePar[3] - slicePar[2];

        // Define the hyperslab offset and slab dimensions
        offset.push_back(slabOffsetRow);
        offset.push_back(slabOffsetCol);
        block.push_back(slabDimRow);
        block.push_back(slabDimCol);
    }

    // For image-series, only return a single image. Perhaps in the future more
    // complicatedimage-stack manipulations could be done, but for now, keep it
    // simple.
    if (object_type == "image-series") {
        // Convert slice notation to the offset and block notation of hdf5
        slabOffsetRow = slicePar[2];
        slabOffsetCol = slicePar[4];
        slabDimRow = slicePar[3] - slicePar[2];
        slabDimCol = slicePar[5] - slicePar[4];

        // Define the hyperslab offset and slab dimensions, which for a
        // single image from an image series corresponds to the first slice
        // parameter, and a dimension of 1.
        offset.push_back(slicePar[0]);
        offset.push_back(slabOffsetRow);
        offset.push_back(slabOffsetCol);
        block.push_back(1);
        block.push_back(slabDimRow);
        block.push_back(slabDimCol);

        // Note the image from the stack that is returned.
        section.push_back(slicePar[0]);
        section.push_back(slicePar[0] + 1);
    }

    // Define the slab
    dataspace::Hyperslab SlabSelection{offset, block};

    // Define the size of the output vector into which data will be read from
    // the selected hyperslab.
    vector <float> HyperslabOutputData;
    HyperslabOutputData.resize(slabDimRow * slabDimCol);

    if (debug) {
        cout << "  This dimension should match when defining a hyper slab:";
        cout << endl;
        cout << "    offset:     " << offset[0]      << " block:      ";
        cout << block[0] << endl;
        cout << "    slabDimRow: " <<  slabDimRow << " slabDimCol: ";
        cout << slabDimCol << endl;
    }

    // Sometimes there's wierd data created, like an array of string values.
    // Try to make some sense out of them here...
    if (IsString) {
        object_type = "text-array";
        try {
            // Try to get the data as if it were a normal string - no bueno
            getScalarOutput datasetResult = getScalar(Dataset, debug);
            error = datasetResult.error;

            // Maybe as an array of string valuie? No - though this works
            // in python....
            vector <string> HyperslabOutputDataString;
            HyperslabOutputDataString.resize(1);
            dataspace::Hyperslab SlabSelectionString{offset, block};
            Dataset.read(HyperslabOutputDataString, SlabSelectionString);

        } catch (std::bad_alloc) {
            error = "Could not read dataset - bad_alloc";
        } catch (std::runtime_error) {
            error = "Could not read dataset - runtime_error";
        }

    // Read the hyperslab data into the output vector
    } else {
        try {
            Dataset.read(HyperslabOutputData, SlabSelection);
        } catch (std::runtime_error) {
            error = "Could not read dataset";
        }
    }

    if (debug) {
        cout << "  error: " << error << endl;
    }

    if (error.compare("False") == 0) {

        if (debug) {
            cout << "  Converting data into vector" << endl;
        }

        // Convert the data that has been read into a two dimensional vector
        int ElementIndex = 0;
        int RowIndex = -1;
        for (auto Value : HyperslabOutputData) {
            if (ElementIndex % slabDimCol == 0) {
                image.push_back(vector<float>());
                RowIndex += 1;
            }
            image[RowIndex].push_back(Value);
            ElementIndex += 1;
        }

        // Note the section of the image that is returned
        section.push_back(slabOffsetRow);
        section.push_back(slabOffsetRow + slabDimRow);
        section.push_back(slabOffsetCol);
        section.push_back(slabOffsetCol + slabDimCol);

        if (debug) {
            cout << "image.size():    " << image.size() << endl;
            if (image.size() > 0 ) {
                cout << "image[0].size(): " << image[0].size() << endl;
            }

            if (Dimensions.size() > 1) {
                cout << "[";
            }
            for (int i=0; i < image.size(); i++) {

                cout << "[";
                for (int j = 0; j < image[i].size(); j++) {

                    cout << image[i][j];
                    if (j < image[i].size() - 1)
                        cout << ", ";
                }
                if (i < image.size() - 1)
                    cout << "], ";
                else
                    cout << "]";
            }
            if (Dimensions.size() > 1) {
                cout << "]";
            }
        }

        // Decimate if necessary
        decimateImageOutput decImOut = decimateImage(image, maxImageSize, debug);

        // Save output
        image_ds = decImOut.image_ds;
        bpr_limits = decImOut.bpr_limits;
        bad_pixels_exist = decImOut.bad_pixels_exist;
        is_downsampled = decImOut.is_downsampled;
    } else {
        image_ds.push_back(vector<float>());
        image_ds[0].push_back(0);

        if (debug) {
            cout << endl;
            cout << "  ***************************************" << endl;
            cout << "  *** Problem occured reading dataset ***" << endl;
            cout << "  ***************************************" << endl;
            cout << endl;
        }
    }

    if (debug) {
        cout << "  bad_pixels_exist: " << bad_pixels_exist << endl;
        cout << "  is_downsampled:   " << is_downsampled << endl;
        cout << endl;
        cout << "***  END  getImage ***" << endl;
        cout << endl;
    }

    return {image_ds, shape, object_type, bpr_limits, section, bad_pixels_exist,
        is_downsampled, error};
}


decimateImageOutput decimateImage(vector< vector <float> > image,
    int maxImageSize, bool debug) {

    /*
     * This function performs a simple decimation of an input 2D array,
     * simply summing the values of neighboring elements, not really taking
     * care of image edges in any smart way.
     */

    if (debug) {
        cout << endl;
        cout << "*** START decimateImage ***" << endl;
        cout << "  maxImageSize:        " << maxImageSize << endl;
        cout << endl;
    }

    // Output variables
    vector < vector <float> > image_ds;
    vector <float> bpr_limits;
    bool bad_pixels_exist = false;
    bool is_downsampled = false;

    // Other variables
    vector < vector <float> > image_ds_bpr;
    vector < vector <bool> > is_bad_pixel;
    int i, j, k;
    int dec_i = -1, dec_j = -1;
    int sum = 0;
    int decimate_size = 0;
    int mean_factor = 0;
    bool perform_downsample = false;
    float max_image = -1;
    float max_image_bpr = -1;
    float min_image_bpr = 4294967295;

    if (image.size() > 0) {

        // Calculate how many pixels of the original image to squish together
        float imageSize = image.size() * image[0].size();
        float scaleFactor = sqrt(imageSize / maxImageSize);

        decimate_size = int(ceil(scaleFactor));
        mean_factor = decimate_size * decimate_size;
    }

    // Determine whether or not to perform decimation
    if (decimate_size > 1) {
        perform_downsample = true;
    }

    if (debug) {
        cout << "    perform_downsample: " << perform_downsample << endl;
        cout << "    decimate_size:      " << decimate_size << endl;
    }

    // See if there are any obviously bad pixels in the image, which have very
    // specific values, depending upon data type
    //   - Those with max values of 4294967295 == maximum value of 32-bit
    //     unsigned integer
    //   - Those with max values of 65535 == maximum value of 16-bit unsigned
    //     integer
    //   - What else? Negative values?
    vector <long int> bad_pixel_values{4294967295, 65535};

    // See if there are any possible bad pixels, which needs to be done prior
    // to decimation, and find the maximum in the image, with and without
    // desregard of such pixels
    for (j=0; j < image.size(); j++) {
        is_bad_pixel.push_back(vector<bool>());
        for (i = 0; i < image[j].size(); i++) {

            bool bad_pixel = false;

            // Find the maximum in the image, ignoring whether or not it's a
            // bad pixel
            if (image[j][i] > max_image) {
                max_image = image[j][i];
            }

            // Loop over possible values for bad pixels, check if this image
            // pixel has one of htose values
            for (k = 0; k < bad_pixel_values.size(); k++) {
                if (image[j][i] == bad_pixel_values[k]) {
                    bad_pixels_exist = true;
                    bad_pixel = true;
                }
            }

            if (!bad_pixel) {
                // Find the minimum and maximum in the image, not including bad
                // pixels
                if (image[j][i] > max_image_bpr) {
                    max_image_bpr = image[j][i];
                }
                if (image[j][i] < min_image_bpr) {
                    min_image_bpr = image[j][i];
                }

            }

            // Make a note of this pixel
            is_bad_pixel[j].push_back(bad_pixel);
        }
    }

    if (debug) {
        cout << "    max_image:          " << max_image << endl;
        cout << "    min_image_bpr:      " << min_image_bpr << endl;
        cout << "    max_image_bpr:      " << max_image_bpr << endl;
        cout << "    bad_pixels_exist:   " << bad_pixels_exist << endl;
        cout << "    image.size():       " << image.size() << endl;
    }

    // If downsampling is required, sum groups of pixels into a new smaller
    // number of pixels
    if (perform_downsample) {

        // Loop over the rows of the image
        for (j=0; j < image.size(); j++) {

            if (debug) {
                cout << "     j: " << j << " % " << decimate_size << ": ";
                cout << j % decimate_size << endl;
            }

            // Create a new row in the decimated image, increase the row index
            if (j % decimate_size == 0) {
                dec_j++;
                image_ds.push_back(vector<float>());
                image_ds_bpr.push_back(vector<float>());
            }

            // Loop over the columns in the image
            for (i = 0; i < image[j].size(); i++) {

                if (debug) {
                    cout << "     i: " << i << " % " << decimate_size << ": ";
                    cout << i % decimate_size << endl;
                }

                // Reset the column index
                if (i == 0) {
                    dec_i = -1;
                }

                // Increase the column index
                if (i % decimate_size == 0) {
                    dec_i++;
                }

                if (debug) {
                    cout << endl;
                    cout << "     dec_j: " << dec_j << ", dec_i: " << dec_i;
                    cout << endl;
                    cout << "     image[" << j << "][" << i << "]: ";
                    cout << image[j][i] << endl;
                    cout << "     mean_factor: " << mean_factor << endl;
                }

                // Create a new column in the decimated image
                if (i % decimate_size == 0 && j % decimate_size == 0) {
                    image_ds[dec_j].push_back(0);
                    image_ds_bpr[dec_j].push_back(0);
                }

                // Add the data to the decimated image, dividing by the square
                // of the scale factor to get the mean
                image_ds[dec_j][dec_i] += image[j][i] / mean_factor;

                if (debug) {
                    cout << "     image_ds[" << dec_j << "][" << dec_i << "]: ";
                    cout << image_ds[dec_j][dec_i] << endl;
                    cout << endl;
                }

                // Fill a separate array with bad pixels removed
                if (bad_pixels_exist) {
                    if (!is_bad_pixel[j][i]) {
                        image_ds_bpr[dec_j][dec_i] += image[j][i] / mean_factor;
                    }
                }

            }
        }

        // Find the max and min after downsampling and with removal of bad
        // pixels
        if (bad_pixels_exist) {

            max_image_bpr = -1;
            min_image_bpr = 4294967295;

            for (j=0; j < image_ds_bpr.size(); j++) {
                for (i = 0; i < image_ds_bpr[j].size(); i++) {

                    if (image_ds_bpr[j][i] > max_image_bpr) {
                        max_image_bpr = image_ds_bpr[j][i];
                    }
                    if (image_ds_bpr[j][i] < min_image_bpr) {
                        min_image_bpr = image_ds_bpr[j][i];
                    }

                }
            }

            if (debug) {
                cout << "min_image_bpr: " << min_image_bpr << endl;
                cout << "max_image_bpr: " << max_image_bpr << endl;
            }
        }

    } else {
        image_ds = image;
    }

    if (image.size() != image_ds.size()) {
        is_downsampled = true;
    }

    // Save the min and max of the image with any bad pixels removed
    bpr_limits.push_back(min_image_bpr);
    bpr_limits.push_back(max_image_bpr);

    if (debug) {

        // Print the original data
        cout << endl;
        cout << "Original data:" << endl;
        for (j=0; j < image.size(); j++) {
            for (i = 0; i < image[j].size(); i++) {
                cout << "     [" << j << "][" << i << "]: ";
                cout << image[j][i] << "  ";
            }
            cout << endl;
        }


        // Print the decimated image
        cout << endl;
        cout << "Decimated data:" << endl;
        for (j=0; j < image_ds.size(); j++) {
            for (i = 0; i < image_ds[j].size(); i++) {
                cout << "     [" << j << "][" << i << "]: ";
                cout << image_ds[j][i] << "  ";
            }
            cout << endl;
        }

        cout << endl;
        cout << "image_ds.size():    " << image_ds.size() << endl;
        if (image_ds.size() > 0) {
            cout << "image_ds[0].size(): " << image_ds[0].size()
                << endl;
        }
        cout << "*** END decimateImage ***" << endl;
        cout << endl;
    }

    return {image_ds, bpr_limits, bad_pixels_exist, is_downsampled};
}
