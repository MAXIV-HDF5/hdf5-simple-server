###############################################################################
# 
#   HDF5 Simple Server Makefile
#   This is meant to be a simple server which is to serve 
#   as the backend for the HDF5 Web GUI, or perhaps other 
#   applications.
#   
#   This Makefile is used for building and running a docker 
#   image of the server, using modifiable configuration files.
#
#  	Run 'make' for usage information
# 
###############################################################################


# Makefile stuff
.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
THIS_FILE := $(lastword $(MAKEFILE_LIST))

export PYTHONPATH=.

# Pretty Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m


# Separator between output, 80 characters wide
define print_separator
	printf "$1"; printf "%0.s*" {1..80}; printf "$(NC)\n"  
endef
print_line_green = $(call print_separator,$(GREEN))
print_line_blue = $(call print_separator,$(BLUE))
print_line_red = $(call print_separator,$(RED))

# Variables for making docker image tags
LAST_COMMIT_DATE = $(shell git log -n 1 --pretty=format:%cd --date=short -- \
	app.py docker/Dockerfile \
	config/hdf5-simple-server-environment.yml \
	resources_cpp/* resources_cpp/src/* resources_cpp/include/* \
	resources_py/*)

LAST_COMMIT_HASH = $(shell git log -n 1 --pretty=format:%h -- \
	app.py docker/Dockerfile \
	config/hdf5-simple-server-environment.yml \
	resources_cpp/* resources_cpp/src/* resources_cpp/include/* \
	resources_py/*)

TAG = $(LAST_COMMIT_DATE)_$(LAST_COMMIT_HASH)

REGISTRY=docker.maxiv.lu.se
PROJECT=maxiv-scisw-hdf5
NAME=hdf5-simple-server

GITLAB_REG=registry.gitlab.com
GITLAB_PROJ=maxiv-scisw/hdf5-viewer/hdf5-simple-server
GITLAB_NAME=hdf5-simple-server


##@ Help - usage and examples
.PHONY: help
help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)HDF5 Simple Server $(CYAN)Makefile$(NC)\n\n"
	printf "    This is meant to be a simple server used as the backend for \n"
	printf "    the HDF5 viewer, or perhaps other applications.\n\n"
	printf "    This Makefile is mainly used for building and running a \n"
	printf "    docker image of the server, using modifiable configuration \n"
	printf "    files. It is also possible to run the server in a \n"
	printf "    development mode with debugging output.\n"
	$(print_line_blue)
	printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Usage\n    $(CYAN)make $(NC)<target>\n"
	printf "\n$(BLUE)Examples$(NC)\n"
	printf "    $(CYAN)make $(NC)build$(NC)\n"
	printf "    $(CYAN)make $(NC)start-demo$(NC)\n"
	printf "    $(CYAN)make $(NC)start-cas-https$(NC)\n"
	printf "    $(CYAN)make $(NC)start-cas-https-maxiv$(NC)\n"
	printf "    $(CYAN)make $(NC)logs$(NC)\n"
	printf "    $(CYAN)make $(NC)stop$(NC)\n"
	# The awk command here is just to make the help message look nice
	@awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-22s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)


##@ Build Image
.PHONY: build 
build: ## Build the docker image, with tag 'local'
	@printf "\n"
	$(print_line_blue)
	docker build -t $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) \
		-f docker/Dockerfile .
	$(print_line_blue)


.PHONY: tag-stable
tag-stable: ## Tag image for use in service 
	docker image tag $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) \
		$(PROJECT)/$(NAME):stable


##@ Start Container 
.PHONY: start-demo
start-demo: ## Start dockerized application using http and no user authentication
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-demo.yml up -d 
	$(print_line_green)


.PHONY: start-demo-maxiv 
start-demo-maxiv: update-user-info ## Use particular config file for MAX IV 
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-demo-maxiv.yml up -d 
	$(print_line_green)


.PHONY: start-cas-https
start-cas-https: update-user-info ## Start dockerized application using https and CAS authenticaion 
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-cas-https.yml up -d 
	$(print_line_green)


.PHONY: start-cas-https-maxiv
start-cas-https-maxiv: update-user-info ## Use particular config file for MAX IV 
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-cas-https-maxiv.yml up -d 
	$(print_line_green)


.PHONY: start-cas-https-jasbru
start-cas-https-jasbru: update-user-info ## Use particular config file for MAX IV
	@printf "\n"
	$(print_line_green)
	docker-compose -f docker/docker-compose-cas-https-jasbru.yml up -d
	$(print_line_green)


.PHONY: update-user-info 
update-user-info: ## Update the list of system and LDAP users used in CAS-HTTPS mode 
	@printf "\n"
	$(print_line_blue)
	getent passwd > user-info/passwd 
	getent group > user-info/group  

	printf "updated local user and group files:\n"
	tree -CDh user-info/
	printf "which can then be used by \n"
	printf "docker/docker-compose-cas-https.yml\n"
	$(print_line_blue)


##@ Stop Container 
.PHONY: stop 
stop: ## Stop the docker container
	@printf "\n"
	$(print_line_red)
	docker-compose -f docker/docker-compose-demo.yml  down
	$(print_line_red)


##@ Container Logs 
.PHONY: logs 
logs: ## Watch the docker logs 
	@printf "\n"
	$(print_line_blue)
	docker-compose -f docker/docker-compose-demo.yml logs -ft	
	$(print_line_blue)


##@ Container Connection 
.PHONY: connect
connect: ## Connect to the running containter as root in a bash session 
	@printf "\n"
	$(print_line_blue)
	docker exec -it maxiv-scisw-hdf5_hdf5-simple-server bash
	$(print_line_blue)


###############################################################################
##@ MAX IV CI 
###############################################################################

.PHONY: push 
push: ## Push image to registry 
	@printf "\n"
	$(print_line_blue)
	docker push $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG)
	$(print_line_blue)


.PHONY: pull 
pull: ## pull image from registry - current tag 
	@printf "\n"
	$(print_line_blue)
	docker pull $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) 
	$(print_line_blue)


.PHONY: list-local
list-local: ## Tag image for push to registry
	@printf "\n"
	$(print_line_blue)
	docker image ls -f reference=$(PROJECT)/$(NAME)
	$(print_line_blue)
	docker image ls -f reference=$(REGISTRY)/$(PROJECT)/$(NAME)
	$(print_line_blue)
	docker image ls -f reference=$(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME)
	$(print_line_blue)


.PHONY: list-registry
list-registry: ## Tag image for push to registry
	@printf "\n"
	$(print_line_blue)
	curl -sX GET https://$(REGISTRY)/v2/$(PROJECT)/$(NAME)/tags/list
	$(print_line_blue)


.PHONY: remove-local
remove-local: ## Remove all local docker images from this project 
	@printf "\n"
	$(print_line_blue)
	docker image ls -f reference=$(PROJECT)/$(NAME) -q \
		| xargs docker image rm -f
	$(print_line_blue)
	docker image ls -f reference=$(REGISTRY)/$(PROJECT)/$(NAME) -q \
		| xargs docker image rm -f
	$(print_line_blue)
	docker image ls -f reference=$(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME) -q \
		| xargs docker image rm -f
	$(print_line_blue)


###############################################################################
##@ GITLAB.COM 
###############################################################################

.PHONY: tag-gitlab-com
tag-gitlab-com: ## Tag image for push public gitlab.com registry
	docker tag $(REGISTRY)/$(PROJECT)/$(NAME):$(TAG) \
		$(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG)


.PHONY: push-gitlab-com
push-gitlab-com: ## Push image to public gitlab.com registry
	@printf "\n"
	$(print_line_blue)
	docker push $(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG)
	$(print_line_blue)


.PHONY: pull-gitlab-com
pull-gitlab-com: ## pull image from gitlab.com registry - current tag 
	@printf "\n"
	$(print_line_blue)
	docker pull $(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG)
	$(print_line_blue)


.PHONY: tag-gitlab-com-stable
tag-gitlab-com-stable: ## Tag image pulled from gitlab.com as stable 
	docker tag $(GITLAB_REG)/$(GITLAB_PROJ)/$(GITLAB_NAME):$(TAG) \
		$(PROJECT)/$(NAME):stable


###############################################################################
##@ Development Setup & Usage
###############################################################################

.PHONY: install-dep-debian install-dep-centos install-h5cpp start-dev-demo start-dev-cas-https venv venv-clean install-conda compile-cpp-code
install-dep-debian: ## Install necessary dependencies for debian
	@printf "\n"
	$(print_line_blue)
	sudo apt-get update \
	  && sudo apt-get install -yq --no-install-recommends \
		cmake \
		curl \
		g++ \
		gcc \
		git \
		hdf5-helpers \
		iputils-ping \
		ldapscripts \
		libboost-all-dev \
		libgtest-dev \
		libhdf5-dev \
		libldap2-dev \
		libsasl2-dev \
		libssl-dev \
		make \
		python-dev \
		tree
	$(print_line_blue)


install-dep-centos: ## Install necessary dependencies for centos - unsuccessfully tested!
	@printf "\n"
	$(print_line_blue)
	sudo yum install -y \
		epel-release
	sudo yum install -y --skip-broken \
		hdf5 \
		hdf5-devel \
		hdf5-static
	sudo yum install -y \
		cmake \
		curl \
		gcc \
		gcc-c++ \
		git \
		iputils \
		boost-devel \
		openldap \
		openssl \
		make \
		python3-devel \
		tree
	$(print_line_blue)


install-h5cpp: ## Install h5cpp
	@printf "\n"
	$(print_line_blue)
	rm -rf h5cpp
	git clone --depth 1 --branch v0.1.3 https://github.com/ess-dmsc/h5cpp.git
	cd h5cpp/ \
	  && mkdir build \
	  && cd build/ \
	  && cmake -DCONAN=DISABLE .. \
	  && make \
	  && sudo make install \
	  && cd ../../ \
	  && rm -rf h5cpp
	$(print_line_blue)


compile-cpp-code: ## Compile the c++ code
	@printf "\n"
	$(print_line_blue)
	cd resources_cpp/ \
	  && ./compile.sh
	$(print_line_blue)


# This command is to sorta check that is installed locally, and if not,
# download it and install it
install-conda:
	@install_path=$(PWD)/venv

	# Check if miniconda has already been installed, do not install if so
	if [ ! -d "$$install_path" ]
	then
		printf "\n"
		$(print_line_blue)
		printf "$(BLUE)Installing $(CYAN)miniconda$(NC)\n"
		$(print_line_blue)
		printf "\n"

		printf "$(LIGHTPURPLE)The directory $$install_path does not exist,\n"
		printf "will now download and install miniconda:\n\n$(NC)"

		miniconda_installer=Miniconda3-py310_23.1.0-1-Linux-x86_64.sh
		miniconda_url="https://repo.anaconda.com/miniconda/$$miniconda_installer"

		# Download and install miniconda
		wget $$miniconda_url
		sh $$miniconda_installer -s -p $$install_path -b

		rm $$miniconda_installer

		$(print_line_blue)
		printf "\n"

	fi


# This command is to sorta check that a python virtualenv exists, and if not,
# create it
venv: ## Install miniconda and python virtual environment
	@install_path=$(PWD)/venv
	printf "\n"

	# Check if the virtualenv has been installed
	if [ ! -d "$$install_path" ]
	then
		# Install miniconda, if necessary
		$(MAKE) -s -f $(THIS_FILE) install-conda

		$(print_line_blue)
		printf "$(BLUE)Installing $(CYAN)virtualenv$(NC)\n"
		$(print_line_blue)
		printf "\n"

		yaml_file=$(PWD)/config/hdf5-simple-server-environment.yml

		## printf "$(BLUE)Installing additional $(CYAN)Python requirements$(NC)"
		printf "$(BLUE)Installing environment for $(CYAN)HDF5 Simple Server$(NC)"
		printf "$(BLUE) from yaml file$(NC)\n"
		printf "    $$yaml_file\n\n"

		# Source the base conda environment
		source $$install_path/bin/activate

		# # Add to base conda environment
		# conda env update --NAME base --file $$yaml_file

		# Create HDF5 simple server environment
		conda env create --file $$yaml_file

	else
		$(print_line_blue)
		printf "The directory "venv/" exists - therefore skipping \n"
		printf "installation of miniconda and virtual environment.\n"
		printf "\n"
		printf "To reinstall miniconda and the virtual environment:\n"
		printf "	make venv-clean\n"
		printf "	make venv\n"
	fi

	$(print_line_blue)
	printf "\n"

venv-clean: ## Remove the virtual environment and miniconda installation
	@printf "\n"
	$(print_line_blue)
	printf "Removing the miniconda installation and virtual environment:\n"
	printf "    rm -rf venv/\n"
	rm -rf venv
	$(print_line_blue)
	printf "\n"


start-dev-demo: ## Start the application with debugging output, not running in docker
	@printf "\n"
	$(print_line_blue)
	if [ "$$UID" == 0 ]
	then
		printf "Hello root\n"
		source venv/bin/activate
		printf "\n"
		printf "Activating conda environment:\n"
		printf "    hdf5-simple-server-environment\n"
		conda activate hdf5-simple-server-environment
		printf "\n"
		printf "Starting application\n"
		$(print_line_blue)
		python app.py -c config/config-dev-demo.cfg -d
	else
		echo "Will run as root"
		sudo $(MAKE) -s -f $(THIS_FILE) start-dev-demo
	fi
	$(print_line_blue)


start-dev-cas-https: ## Start the application with debugging output, not running in docker
	@printf "\n"
	$(print_line_blue)
	if [ "$$UID" == 0 ]
	then
		printf "Hello root\n"
		source venv/bin/activate
		printf "\n"
		printf "Activating conda environment:\n"
		printf "    hdf5-simple-server-environment\n"
		conda activate hdf5-simple-server-environment
		printf "\n"
		printf "Starting application\n"
		$(print_line_blue)
		python app.py -c config/config-dev-cas-https-jasbru.cfg -d
	else
		echo "Will run as root"
		sudo $(MAKE) -s -f $(THIS_FILE) start-dev-cas-https
	fi
	$(print_line_blue)


###############################################################################
##@ Errors
###############################################################################

.PHONY: cgroup-error-fix
cgroup-error-fix: ## Fix cgroup mountpoint docker error! Debian only issue?
	@$(print_line_green)

	systemd_dir=/sys/fs/cgroup/systemd
	if [ ! -d "$$systemd_dir" ]; then
		printf "$(GREEN)**$(CYAN) Fixing Error:$(NC)\n"
		printf "$(GREEN)**   $(RED) cgroup mountpoint does not exist: unknown$(NC)\n"
		printf "    \n"

		printf "$(RED)  $$systemd_dir does not exist\n$(NC)"
		printf "  Will create it now:\n"
		sudo mkdir $$systemd_dir
		sudo mount -t cgroup -o none,name=systemd cgroup $$systemd_dir

		printf "$(GREEN)**$(CYAN) Done Fixing Error$(NC)\n"
	else
		printf "$(GREEN)  $$systemd_dir exists    \n$(NC)"
		printf "  Maybe the issue is something else?\n"
	fi

	$(print_line_green)
